
const express = require('express');
const bodyParser = require('body-parser');
const { request } = require('express');
const app = express();



app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.listen(80);
require('./app/routes')(app);
console.log("Server started at 80");
