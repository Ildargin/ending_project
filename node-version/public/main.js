$(document).ready(function() {
	$('#butsave').on('click', function() {
		$("#butsave").attr("disabled", "disabled");
		var name = $('#name').val();
		var mail = $('#mail').val();
		var tel = $('#tel').val();
    var subject = $('#subject').val();
    var btn = document.getElementById("butsave");
		if(name!="" && mail!="" && tel!="" && subject!=""){
			$.ajax({
        method: "POST",
        url: "/user",
				data:JSON.stringify({
					name: name,
					mail: mail,
					tel: tel,
					subject: subject				 
        }),
        cache: false,
        dataType: "json",
        contentType: "application/json"    
      });
      document.getElementById("contact-form").reset();
      document.getElementById("users_table").innerHTML = '';
    }
		else{
      alert('Please fill all the field !');
    }
    btn.disabled = false;
  });
  

  $('#gd').on('click', function() {
    $.getJSON("/getdata", function (response) {
      let obj=response;
      var html = "";
      console.log(response);
      for(var a = 0; a < obj.length; a++) {
        var id=  obj[a].id;
        var name = obj[a].name;
        var mail = obj[a].email;
        var tel = obj[a].tel;
        var subj= obj[a].subjec
        html += "<tr>";
            html += "<th>" + id + "</th>";
            html += "<td>" + name + "</td>";
            html += "<td>" + mail + "</td>";
            html += "<td>" + tel + "</td>";
        html += "</tr>";
      }
      document.getElementById("users_table").innerHTML += html;   
    });    
  });
});
