$(document).ready(function() {
	$('#butsave').on('click', function() {
		$("#butsave").attr("disabled", "disabled");
		var name = $('#name').val();
		var mail = $('#mail').val();
		var tel = $('#tel').val();
    var subject = $('#subject').val();
    var btn = document.getElementById("butsave");
		if(name!="" && mail!="" && tel!="" && subject!=""){
			$.ajax({
				url: "send.php",
				type: "POST",
				data: {
					name: name,
					mail: mail,
					tel: tel,
					subject: subject				
				},
				cache: false,
      });
      document.getElementById("contact-form").reset();
      document.getElementById("users_table").innerHTML = '';
    }
		else{
      alert('Please fill all the field !');
    }
    btn.disabled = false;
	});
});

function getProfile() {
  document.getElementById("users_table").innerHTML = '';
  var ajax = new XMLHttpRequest();
  ajax.open("GET", "get.php", true);
  ajax.send();
  
  ajax.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        
          var resp = this.responseText;
          var obj = jQuery.parseJSON(resp);
          var html = "";
          for(var a = 0; a < obj.length; a++) {
              var id=  obj[a].id;
              var name = obj[a].name;
              var mail = obj[a].email;
              var tel = obj[a].tel;
              var subj= obj[a].subject;

              html += "<tr>";
                  html += "<th>" + id + "</th>";
                  html += "<td>" + name + "</td>";
                  html += "<td>" + mail + "</td>";
                  html += "<td>" + tel + "</td>";
                  html += "<td>" + subj + "</td>";
              html += "</tr>";
          }
          document.getElementById("users_table").innerHTML += html;
      }
  };
}


